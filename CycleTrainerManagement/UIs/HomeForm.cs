﻿using CycleTrainerManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CycleTrainerManagement.UIs
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            //Thread t = new Thread(new ThreadStart(Loading));
            //t.Start();
            //Thread.Sleep(5000);
            InitializeComponent();
            //t.Abort();
        }

        private static HomeForm _Form = null;
        public static ReadInfo Info = new ReadInfo();

        public static HomeForm Instance(ReadInfo info)
        {
            if (_Form == null)
            {
                Info = info;
                _Form = new HomeForm();
            }
            return _Form;
        }

        private void cycleDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HomeForm_Load(object sender, EventArgs e)
        {


            GetCycleData(Info);
            GetBasicInfo(Info);


        }

        private void GetCycleData(ReadInfo info)
        {
            List<HrData> ListHrData = new List<HrData>();
            ListHrData = info.HrDataList;
            cycleDataGridView.DataSource = null;
            cycleDataGridView.DataSource = ListHrData;
        }

        private void GetBasicInfo(ReadInfo info)
        {
            List<ParamInfos> basicInfoList = new List<ParamInfos>();
            ParamInfos paramInfo = new ParamInfos();
            paramInfo = info.Params;
            basicInfoList.Add(paramInfo);
            gridViewBasicInfo.DataSource = null;
            gridViewBasicInfo.DataSource = basicInfoList;
        }

        private void HomeForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _Form = null;
        }
    }
}
