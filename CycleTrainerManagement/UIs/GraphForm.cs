﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using CycleTrainerManagement.Models;

namespace CycleTrainerManagement.UIs
{
    public partial class GraphForm : Form
    {
        public GraphForm()
        {
            InitializeComponent();
        }
        public static List<HrData> Info = null;

        private static GraphForm _Form = null;
        public static GraphForm Instance(List<HrData> hrList)
        {
            if (_Form == null)
            {
                Info = hrList;
                _Form = new GraphForm();
            }
            return _Form;
        }

        private void GraphForm_Load(object sender, EventArgs e)
        {
            CreateGraph(zedGraphCycle, Info, true, true, true, true, true, true);
        }
        private void CreateGraph(ZedGraphControl zgc, List<HrData> hr, bool HrGraph, bool SpeedGraph, bool CadenceGraph, bool AltitudeGraph, bool PowerGraph, bool BalaceGraph)
        {
            zgc.GraphPane.CurveList.Clear();
            zgc.GraphPane.GraphObjList.Clear();
            GraphPane myPane = zgc.GraphPane;
            myPane.Title.Text = "Cycle Report";
            myPane.XAxis.Title.Text = "Data";
            myPane.YAxis.Title.Text = "Value";
            var x = 0;
            if (HrGraph)
            {
                PointPairList listHr = new PointPairList();
                foreach (var item in hr)
                {
                    listHr.Add(x, double.Parse(item.HeartRate));
                    x++;
                }
                x = 0;
                LineItem myCurveHr = myPane.AddCurve("HeartRate", listHr, Color.Blue,
                                        SymbolType.Triangle);
            }
            //myCurveHr.Line.Fill = new Fill(Color.White, Color.Red, 45F);
            //myCurveHr.Symbol.Fill = new Fill(Color.White);
            //myPane.Chart.Fill = new Fill(Color.White, Color.LightGoldenrodYellow, 45F);
            //myPane.Fill = new Fill(Color.White, Color.FromArgb(220, 220, 255), 45F);

            if (SpeedGraph)
            {
                PointPairList listSpeed = new PointPairList();
                foreach (var item in hr)
                {
                    listSpeed.Add(x, double.Parse(item.SpeedInKMH));
                    x++;
                }
                x = 0;
                LineItem myCurveSpeed = myPane.AddCurve("Speed", listSpeed, Color.Red,
                                        SymbolType.Star);
            }

            if (AltitudeGraph)
            {
                PointPairList listAltitude = new PointPairList();
                foreach (var item in hr)
                {
                    listAltitude.Add(x, double.Parse(item.Altitude));
                    x++;
                }
                x = 0;
                LineItem myCurveAlt = myPane.AddCurve("Altitude", listAltitude, Color.Green,
                                        SymbolType.Square);
            }


            if (PowerGraph)
            {
                PointPairList listPower = new PointPairList();
                foreach (var item in hr)
                {
                    listPower.Add(x, double.Parse(item.PowerInWatt));
                    x++;
                }
                x = 0;
                LineItem myCurvePower = myPane.AddCurve("Power", listPower, Color.Magenta,
                                        SymbolType.Circle);
            }

            if (CadenceGraph)
            {

                PointPairList listCadence = new PointPairList();
                foreach (var item in hr)
                {
                    listCadence.Add(x, double.Parse(item.Cadence));
                    x++;
                }
                x = 0;
                LineItem myCurveCadence = myPane.AddCurve("Cadence", listCadence, Color.Purple,
                                        SymbolType.Diamond);
            }

            //PointPairList listPaddalBalance = new PointPairList();
            //foreach (var item in hr)
            //{
            //    listPaddalBalance.Add(x, double.Parse(item.PowerBalancePaddalIndex));
            //    x++;
            //}
            //x = 0;
            //LineItem myCurvePaddalBalance= myPane.AddCurve("Power Balance", listPaddalBalance, Color.Plum,
            //                        SymbolType.Default);


            zgc.AxisChange();
            zgc.Invalidate();

            zgc.Refresh();
        }

        private void GraphForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _Form = null;
        }

        private void checkBoxHr_CheckedChanged(object sender, EventArgs e)
        {
            ReDrawFilterGraph();


        }

        private void checkBoxSpeed_CheckedChanged(object sender, EventArgs e)
        {
            ReDrawFilterGraph();

        }

        private void checkBoxCadence_CheckedChanged(object sender, EventArgs e)
        {
            ReDrawFilterGraph();

        }

        private void checkBoxAltitude_CheckedChanged(object sender, EventArgs e)
        {
            ReDrawFilterGraph();
        }

        private void checkBoxBalace_CheckedChanged(object sender, EventArgs e)
        {
            //CreateGraph(zedGraphCycle, Info, true, true, true, true, true, checkBoxBalace.Checked);
            //zedGraphCycle.AxisChange();
            //zedGraphCycle.Invalidate();
            //zedGraphCycle.Refresh();
        }

        private void checkBoxPower_CheckedChanged(object sender, EventArgs e)
        {
            ReDrawFilterGraph();
        }

        private void ReDrawFilterGraph()
        {
            CreateGraph(zedGraphCycle, Info, checkBoxHr.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked, checkBoxAltitude.Checked, checkBoxPower.Checked, true);
            zedGraphCycle.AxisChange();
            zedGraphCycle.Invalidate();
            zedGraphCycle.Refresh();
        }
    }
}
