﻿namespace CycleTrainerManagement.UIs
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zedGraphCycle = new ZedGraph.ZedGraphControl();
            this.checkBoxHr = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxCadence = new System.Windows.Forms.CheckBox();
            this.checkBoxAltitude = new System.Windows.Forms.CheckBox();
            this.checkBoxPower = new System.Windows.Forms.CheckBox();
            this.checkBoxBalace = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // zedGraphCycle
            // 
            this.zedGraphCycle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zedGraphCycle.Location = new System.Drawing.Point(14, 49);
            this.zedGraphCycle.Name = "zedGraphCycle";
            this.zedGraphCycle.ScrollGrace = 0D;
            this.zedGraphCycle.ScrollMaxX = 0D;
            this.zedGraphCycle.ScrollMaxY = 0D;
            this.zedGraphCycle.ScrollMaxY2 = 0D;
            this.zedGraphCycle.ScrollMinX = 0D;
            this.zedGraphCycle.ScrollMinY = 0D;
            this.zedGraphCycle.ScrollMinY2 = 0D;
            this.zedGraphCycle.Size = new System.Drawing.Size(1416, 491);
            this.zedGraphCycle.TabIndex = 0;
            // 
            // checkBoxHr
            // 
            this.checkBoxHr.AutoSize = true;
            this.checkBoxHr.Checked = true;
            this.checkBoxHr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHr.Font = new System.Drawing.Font("Tw Cen MT Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHr.Location = new System.Drawing.Point(14, 13);
            this.checkBoxHr.Name = "checkBoxHr";
            this.checkBoxHr.Size = new System.Drawing.Size(102, 26);
            this.checkBoxHr.TabIndex = 2;
            this.checkBoxHr.Text = "Heart Rate";
            this.checkBoxHr.UseVisualStyleBackColor = true;
            this.checkBoxHr.CheckedChanged += new System.EventHandler(this.checkBoxHr_CheckedChanged);
            // 
            // checkBoxSpeed
            // 
            this.checkBoxSpeed.AutoSize = true;
            this.checkBoxSpeed.Checked = true;
            this.checkBoxSpeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSpeed.Font = new System.Drawing.Font("Tw Cen MT Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSpeed.Location = new System.Drawing.Point(140, 13);
            this.checkBoxSpeed.Name = "checkBoxSpeed";
            this.checkBoxSpeed.Size = new System.Drawing.Size(68, 26);
            this.checkBoxSpeed.TabIndex = 3;
            this.checkBoxSpeed.Text = "Speed";
            this.checkBoxSpeed.UseVisualStyleBackColor = true;
            this.checkBoxSpeed.CheckedChanged += new System.EventHandler(this.checkBoxSpeed_CheckedChanged);
            // 
            // checkBoxCadence
            // 
            this.checkBoxCadence.AutoSize = true;
            this.checkBoxCadence.Checked = true;
            this.checkBoxCadence.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCadence.Font = new System.Drawing.Font("Tw Cen MT Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCadence.Location = new System.Drawing.Point(226, 13);
            this.checkBoxCadence.Name = "checkBoxCadence";
            this.checkBoxCadence.Size = new System.Drawing.Size(83, 26);
            this.checkBoxCadence.TabIndex = 4;
            this.checkBoxCadence.Text = "Cadence";
            this.checkBoxCadence.UseVisualStyleBackColor = true;
            this.checkBoxCadence.CheckedChanged += new System.EventHandler(this.checkBoxCadence_CheckedChanged);
            // 
            // checkBoxAltitude
            // 
            this.checkBoxAltitude.AutoSize = true;
            this.checkBoxAltitude.Checked = true;
            this.checkBoxAltitude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAltitude.Font = new System.Drawing.Font("Tw Cen MT Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAltitude.Location = new System.Drawing.Point(330, 13);
            this.checkBoxAltitude.Name = "checkBoxAltitude";
            this.checkBoxAltitude.Size = new System.Drawing.Size(83, 26);
            this.checkBoxAltitude.TabIndex = 5;
            this.checkBoxAltitude.Text = "Altitude";
            this.checkBoxAltitude.UseVisualStyleBackColor = true;
            this.checkBoxAltitude.CheckedChanged += new System.EventHandler(this.checkBoxAltitude_CheckedChanged);
            // 
            // checkBoxPower
            // 
            this.checkBoxPower.AutoSize = true;
            this.checkBoxPower.Checked = true;
            this.checkBoxPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPower.Font = new System.Drawing.Font("Tw Cen MT Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxPower.Location = new System.Drawing.Point(434, 13);
            this.checkBoxPower.Name = "checkBoxPower";
            this.checkBoxPower.Size = new System.Drawing.Size(71, 26);
            this.checkBoxPower.TabIndex = 6;
            this.checkBoxPower.Text = "Power";
            this.checkBoxPower.UseVisualStyleBackColor = true;
            this.checkBoxPower.CheckedChanged += new System.EventHandler(this.checkBoxPower_CheckedChanged);
            // 
            // checkBoxBalace
            // 
            this.checkBoxBalace.AutoSize = true;
            this.checkBoxBalace.Checked = true;
            this.checkBoxBalace.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBalace.Font = new System.Drawing.Font("Tw Cen MT Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBalace.Location = new System.Drawing.Point(524, 13);
            this.checkBoxBalace.Name = "checkBoxBalace";
            this.checkBoxBalace.Size = new System.Drawing.Size(121, 26);
            this.checkBoxBalace.TabIndex = 7;
            this.checkBoxBalace.Text = "Padal Balance";
            this.checkBoxBalace.UseVisualStyleBackColor = true;
            this.checkBoxBalace.CheckedChanged += new System.EventHandler(this.checkBoxBalace_CheckedChanged);
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 553);
            this.Controls.Add(this.checkBoxBalace);
            this.Controls.Add(this.checkBoxPower);
            this.Controls.Add(this.checkBoxAltitude);
            this.Controls.Add(this.checkBoxCadence);
            this.Controls.Add(this.checkBoxSpeed);
            this.Controls.Add(this.checkBoxHr);
            this.Controls.Add(this.zedGraphCycle);
            this.Font = new System.Drawing.Font("Lucida Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GraphForm";
            this.Text = "GraphForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GraphForm_FormClosed);
            this.Load += new System.EventHandler(this.GraphForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphCycle;
        private System.Windows.Forms.CheckBox checkBoxHr;
        private System.Windows.Forms.CheckBox checkBoxSpeed;
        private System.Windows.Forms.CheckBox checkBoxCadence;
        private System.Windows.Forms.CheckBox checkBoxAltitude;
        private System.Windows.Forms.CheckBox checkBoxPower;
        private System.Windows.Forms.CheckBox checkBoxBalace;
    }
}