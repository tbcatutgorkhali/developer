﻿using CycleTrainerManagement.DataReader;
using CycleTrainerManagement.Models;
using CycleTrainerManagement.UIs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
namespace CycleTrainerManagement
{
    public partial class FrmCycleManagerHome : Form
    {
        public FrmCycleManagerHome()
        {
            InitializeComponent();
        }

        public void Loading()
        {
            try
            {
                Application.Run(new LoadingScreen());
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void menuStripStart_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(Loading));
            t.Start();
            Thread.Sleep(1000);
            LoadData();
            t.Abort();
        }
        private void LoadData()
        {
            try
            {
                ReadInfo info = GetData();
                HomeForm _homeForm = HomeForm.Instance(info);
                _homeForm.MdiParent = this;
                _homeForm.Show();
            }
            catch (Exception)
            {

                MessageBox.Show("Error! while loading data!");
            }
        }

        private static ReadInfo GetData()
        {
            ReadInfo info = new ReadInfo();
            try
            {

                info = DataLoader.Reader();
                return info;

            }
            catch (Exception)
            {
                MessageBox.Show("Error! while reading data!.");
                return info;
            }
        }

        private void graphMenuToolStrip_Click(object sender, EventArgs e)
        {
            try
            {
                ReadInfo info = GetData();
                List<HrData> ListHrData = new List<HrData>();
                ListHrData = info.HrDataList;
                GraphForm _graphForm = GraphForm.Instance(ListHrData);
                _graphForm.MdiParent = this;
                _graphForm.Show();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void summaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ReadInfo info = GetData();
                SummaryForm _graphForm = SummaryForm.Instance(info);
                _graphForm.MdiParent = this;
                _graphForm.Show();
            }
            catch (Exception)
            {

                MessageBox.Show("Something went wrong when loading summery!");
            }
        }

        private void FrmCycleManagerHome_Load(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
